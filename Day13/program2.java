
// minimum distance between two numbers [gfg]

class Solution {
    int minDist(int a[], int n, int x, int y) {

        int ans = Integer.MAX_VALUE;
        int first = -1;
        int sec =  -1;

        for(int i=0; i<n; i++){
            if(a[i]==x){
                first = i;
            }
            if(a[i]==y){
                sec = i;
            }
            if(first != -1 && sec != -1){
                //ans = Math.min(Math.abc(first-sec),ans);
                if( ans > (first - sec) && first-sec >0 ){
                    ans = first-sec;
                }
                if( ans > (sec-first) && sec-first >0){
                    ans = sec-first;
                }
            }
        }

        if(first == -1 || sec == -1){
            ans = -1;
        }
        return ans;
     }

    public static void main(String[] args){
    
	Solution obj = new Solution();
	
	int arr[] = new int[]{1,2,3,2,2,4,1};
	int x = 1;
	int y= 2;

	System.out.println(obj.minDist(arr,arr.length,x,y));
    }
}
