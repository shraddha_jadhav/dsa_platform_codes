
// plus One [Leetcode-66]

class Solution {
    public int[] plusOne(int[] digits) {

        for(int i=digits.length-1; i >=0; i--){

            if(digits[i]==9){
                digits[i] = 0;
            }else{
                digits[i]++;
                return digits;
            }
        }

        int nums[] = new int[digits.length+1];
        nums[0] = 1;
       for(int i=0; i<digits.length; i++){
            nums[i+1] = digits[i];
        }
        return nums;
    }

    public static void main(String[] args){
    
	    Solution obj = new Solution();

	    int digits[] = new int[]{1,2,6,7,8};
	    
	    int num[] = obj.plusOne(digits);

	    for(int i=0; i<num.length; i++){
	    
		    System.out.print(num[i] + " ");
	    }

	    System.out.println();
    }
}
