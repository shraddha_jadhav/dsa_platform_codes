
// Count Special Quadruplets

class Solution {
    public int countQuadruplets(int[] nums) {

        int n = nums.length;
        int count =0;

        for(int a =0; a<n; a++){
            for(int b=a+1; b <n; b++){
                for(int c = b+1; c<n; c++){
                    for(int d=c+1; d<n; d++){
                        if(nums[a]+nums[b]+nums[c]==nums[d]){
                            count++;
                        }
                    }
                }
            }
        }
        return count;
    }

    public static void main(String[] args){
    
	    int nums[] = new int[]{1,2,3,6};

	    Solution obj = new Solution();
	    System.out.println(obj.countQuadruplets(nums));
    }
}
