
// Bubble Sort [gfg]

class Solution{

     public static void bubbleSort(int arr[], int n){
 
	for(int i=0; i<n-1; i++){
            for(int j=0; j<n-1-i; j++){
                if(arr[j]>arr[j+1]){
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }
        }
    }

    public static void main(String[] args){
    
	    int arr[] = new int[]{6,2,8,2,1,3,5};

	    bubbleSort(arr,arr.length);

	    for(int i=0; i<arr.length; i++){
	    
		    System.out.print(arr[i] + " ");
	    }
	    System.out.println();
    }
}
