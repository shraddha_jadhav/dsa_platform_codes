
// Transpose of Matrix [gfg]

class Solution{

    public void transpose(int n,int a[][])   {
        
        for(int i=0; i<n; i++){
            for(int j=i+1; j<n; j++){
                int temp = a[i][j];
                a[i][j] = a[j][i];
                a[j][i] = temp;
            }
        }
    }

    public static void main(String[] args){
    
	    int a[][] = new int[][]{{1,1,1,1},{2,2,2,2},{3,3,3,3},{4,4,4,4}};

	    Solution obj = new Solution();
	    obj.transpose(a[0].length, a);

	    for(int i=0; i<a[0].length; i++){
	    
		    for(int j=0; j<a.length; j++){
		    
			    System.out.print(a[i][j] + " ");
		    }
		    System.out.println();
	    }
    }
}

