
// Array subset of another array (gfg)
import java.util.*;
class Compute {

    public String isSubset( long a1[], long a2[], long n, long m) {
        int i=0;
        int j=0;
        if(n<m){
            return "No";
        }

        Arrays.sort(a1);
        Arrays.sort(a2);

        while(i<m && j<n){
            if(a1[j]<a2[i]){
                j++;
            }else if(a1[j]==a2[i]){
                i++;
                j++;
            }else{
                return "No";
            }
        }
        if(i<m){
            return "No";
        }else{
            return "Yes";
        }

    }

    public static void main(String[] args){
    
	    Compute obj = new Compute();

	    long a1[] = new long[]{1,3,5,2,6,9};
	    long a2[] = new long[]{2,6,9};

	    String str = obj.isSubset(a1,a2,a1.length,a2.length);
	    System.out.println(str);
    }
}
