
class Solution {
    public int[][] construct2DArray(int[] original, int m, int n) {
        int result[][] = new int[m][n];
        if(original.length != m*n){
            return new int[0][0];
        }

        int k = 0;

        int totalElemets = original.length;

        for(int i=0; i<m; i++){
            for(int j=0; j<n; j++){
                result[i][j] = original[k];
                k++;
            }
        }
        return result;
    }

    public static void main(String[] args){
    
	    Solution obj = new Solution();

	    int original[] = new int[]{1,2,3,4};

	    int ret[][] = obj.construct2DArray(original,2,2);
		
	    for(int i=0; i<ret.length; i++){
	    
		    for(int j=0; j<ret[0].length; j++){
		    
			    System.out.print(ret[i][j] + " ");
		    }
		    System.out.println();
	    }

    }
}
