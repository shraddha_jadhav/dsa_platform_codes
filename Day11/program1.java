
// Running Sum of 1D array (Leetcode=1480)

class Solution {

    public int[] runningSum(int[] nums) {

        for(int i=1; i<nums.length; i++){

            nums[i] = nums[i]+nums[i-1];

        }

        return nums;

    }

    public static void main(String[] args){
    
	    Solution obj = new Solution();
	    int nums[] = new int[]{1,2,3,4,5};

	    obj.runningSum(nums);

	    for(int i=0; i<nums.length; i++){
	    
		    System.out.print(nums[i] + " ");
	    }

	    System.out.println();
    }

}
