
// Equilibrium Point

class Solution {
    // a: input array
    // n: size of array
    // Function to find equilibrium point in the array.
    public static int equilibriumPoint(long arr[], int n) {

        // Your code here
        long arr2[] = new long[arr.length];
       arr2[0] = arr[0];

       for(int i=1; i<arr.length; i++){
           arr2[i] = arr2[i-1] + arr[i];
       }

       for(int i=0; i<arr.length; i++){

           if(i !=0 && arr2[i-1]==arr2[arr.length-1]-arr2[i]){
               return i+1;
           }

           if(i==0 && arr2[arr.length-1]-arr2[0]==0){
               return 1;
           }

           if(i==arr.length-1 && arr2[arr.length-2]==0){
               return arr.length;
           }
       }
       return -1;
    }

    public static void main(String[] args){

            Solution obj = new Solution();

            long nums[] = new long[]{1,7,3,6,5,6};

            int ret = obj.equilibriumPoint(nums,nums.length);
            System.out.println(ret);

    }

}
