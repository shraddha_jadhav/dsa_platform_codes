
// Find pivot Index

class Solution {

    public int pivotIndex(int[] nums) {
       int arr[] = new int[nums.length];
       arr[0] = nums[0];

       for(int i=1; i<nums.length; i++){
           arr[i] = arr[i-1] + nums[i];
       }

       for(int i=0; i<arr.length; i++){

           if(i !=0 && arr[i-1]==arr[nums.length-1]-arr[i]){
               return i;
           }

           if(i==0 && arr[nums.length-1]-arr[0]==0){
               return 0;
           }

           if(i==nums.length-1 && arr[nums.length-2]==0){
               return nums.length-1;
           }
       }
       return -1;
    }

    public static void main(String[] args){
    
	    Solution obj = new Solution();

	    int nums[] = new int[]{1,7,3,6,5,6};

	    int ret = obj.pivotIndex(nums);
	    System.out.println(ret);
    }
}
