
// Equal Left and Right Subarray Sum (gfg)

class Solution{
	int equalSum(int [] A, int N) {
		//Write your code here
	   int arr[] = new int[N];
       arr[0] = A[0];

       for(int i=1; i<N; i++){
           arr[i] = arr[i-1] + A[i];
       }

       for(int i=0; i<arr.length; i++){

           if(i !=0 && arr[i-1]==arr[N-1]-arr[i]){
               return i+1;
           }

           if(i==0 && arr[N-1]-arr[0]==0){
               return 1;
           }

           if(i==N-1 && arr[N-2]==0){
               return N;
           }
       }
       return -1;
     }

     public static void main(String[] args){

            Solution obj = new Solution();

            int nums[] = new int[]{1,7,3,6,5,6};

            int ret = obj.equalSum(nums,nums.length);
            System.out.println(ret);

    }

}
