
// Set Matrix zeroes [Leetcode-73,GFG]
import java.util.*;
class Solution {
    public void setZeroes(int[][] matrix) {
		int n = matrix.length;
		int m = matrix[0].length;

		List<Integer> row = new ArrayList<Integer>();
        List<Integer> col = new ArrayList<Integer>();

        for(int i=0; i<n; i++){
            for(int j=0; j<m; j++){
                if(matrix[i][j]==0){
                    row.add(i);
                    col.add(j);
                }
            }
        }

        for(int x: row){
            Arrays.fill(matrix[x],0);
        }

        for(int x: col){
            for(int i=0; i<n; i++){
                matrix[i][x] = 0;
            }
        }
    }

    public static void main(String[] args){
    
	    int mat[][]= new int[][]{{1,3,0},{0,2,4},{44,6,33}};

	    for(int i=0; i<mat.length; i++){

                    for(int j=0; j<mat[0].length; j++){

                            System.out.print(mat[i][j] + " ");
                    }
		    System.out.println();
            }

	    System.out.println("Output");


	    Solution obj = new Solution();
	    obj.setZeroes(mat);

	    for(int i=0; i<mat.length; i++){
	    
		    for(int j=0; j<mat[0].length; j++){
		    
			    System.out.print(mat[i][j] + " ");
		    }
		    System.out.println();
	    }
    }
}
