
// Search in a matrix [Leetcode-74,GFG]

class Sol
{
    public static int matSearch(int mat[][], int N, int M, int X)
    {
        // your code here

        for(int i=0; i<N; i++){
            for(int j=0; j<M; j++){
                if(mat[i][j]==X){
                    return 1;
                }
            }
        }
        return 0;
    }

    public static void main(String[] args){
    
	    int mat[][] = new int[][]{{3,30,38},{44,33,77},{2,55,11}};

	    System.out.println(matSearch(mat,mat.length,mat[0].length,38));
    }
}
