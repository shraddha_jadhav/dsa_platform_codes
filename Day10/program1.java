
// Squares of sorted array (LeetCode)

import java.util.*;
class Solution{

    public int[] sortedSquares(int[] nums) {
        
        for(int i=0;i<nums.length; i++){
            nums[i] = nums[i]*nums[i];
        }

        Arrays.sort(nums);
        return nums;
    }

    public static void main(String[] args){
    
	    int nums[] = new int[]{1,2,3,0,5};

	    Solution obj = new Solution();
	    int arr[] = obj.sortedSquares(nums);

	    for(int i=0; i<arr.length; i++){
	    
		    System.out.print(arr[i] + " ");
	    }

	    System.out.println();

    }
}
