
//Product array puzzle (GFG)

class Solution 
{ 
	public static long[] productExceptSelf(int nums[], int n) { 
        // code here
        long product = 1;
        long arr[] = new long[n];

        int count = 0;
        int idx = -1;
        for(int i=0; i<n; i++){
            if(nums[i] != 0){
                product *= nums[i];
            }else{
                count++;
                idx = i;
            }
        }

        for(int i=0; i<n; i++){
            if(count > 1){
                break;
            }else if(count == 1){
                arr[idx] = product;
                break;
            }else{
                arr[i] = (long)(product/nums[i]);
            }
        }
        return arr;
    }

   public static void main(String[] args){

            int nums[] = new int[]{1,2,3,0,5};

            long arr[] = productExceptSelf(nums,nums.length);

            for(int i=0; i<arr.length; i++){

                    System.out.print(arr[i] + " ");
            }

            System.out.println();
    }

} 

