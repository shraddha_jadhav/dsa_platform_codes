
// Longest Common prefix in a Array

class Solution {
    public String longestCommonPrefix(String[] strs) {
        String prefix = "";
        if(strs.length == 1){
            return new String(strs[0]);
        }
        if(strs==null || strs.length==0){
            return prefix;
        }

        for(int i=0; i<strs.length-1; i++){
            int idx = 0;
            String temp = "";
            while(strs[i].length() >idx && strs[i+1].length()>idx){
                if(strs[i].charAt(idx)==strs[i+1].charAt(idx)){
                    if(i==0 || (prefix.length()>idx && prefix.charAt(idx)==strs[i].charAt(idx))){
                        temp = temp+strs[i].charAt(idx);
                    }else{
                        break;
                    }
                    idx++;
                }else{
                    break;
                }
            }
            if(temp.length()==0){
                return "";
            }
            prefix = temp;
        }
        return prefix;
    }

    public static void main(String[] args){
    
	    Solution obj = new Solution();

	    String strs[] = new String[]{"flower","flow","flight"};
	    System.out.println(obj.longestCommonPrefix(strs));
    }
}
