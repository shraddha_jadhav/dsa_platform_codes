
// Subarray sum Equals k [Leetcode-560]

class Solution {
    public int subarraySum(int[] nums, int k) {
        int count = 0;
        for(int i=0; i<nums.length; i++){
            int sum = 0;
            for(int j=i; j<nums.length; j++){
                sum = sum + nums[j];
                if(sum==k){
                    count++;
                }
            }
        }
        return count;
    }

    public static void main(String[] args){
    
	    int arr[] = new int[]{1,2,3,4,5,6};
	    int k = 12;

	    Solution obj = new Solution();
	    System.out.println(obj.subarraySum(arr,k));
    }
}
