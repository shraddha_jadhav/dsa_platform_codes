
// Maximum of all subarrays of size k [gfg]
import java.util.*;
class Solution{

     static ArrayList <Integer> max_of_subarrays(int arr[], int n, int k){
       
        ArrayList<Integer> lst = new ArrayList<Integer>();

        for(int i=0; i<n-k+1; i++){
            int max = 0;
            for(int j=i; j<i+k; j++){
                if(max < arr[j]){
                    max = arr[j];
                }
            }
            lst.add(max);
        }
        return lst;
    }

    public static void main(String[] args){
    
	    int arr[] = new int[]{2,4,7,3,4,5,6};
	    int k = 3;

	    ArrayList<Integer> lst = max_of_subarrays(arr,arr.length,k);

	    System.out.println(lst);

    }
}
