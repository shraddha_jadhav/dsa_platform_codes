
// Find missing and repeating numbers [gfg]

class Solve {
    int[] findTwoElement(int arr[], int n) {

        int countArr[] = new int[n+1];

        for(int i=0; i<n; i++){
            countArr[arr[i]]++;
        }
        int ans[] = new int[2];
        for(int i=1; i<countArr.length; i++){
            if(countArr[i] > 1){
                ans[0] = i;
            }

            if(countArr[i]==0){
                ans[1] = i;
            }
        }
        return ans;
    }

    public static void main(String[] args){
    
	    int arr[] = new int[]{2,3,5,1,1};

	    Solve obj = new Solve();
	    int ans[] = obj.findTwoElement(arr,arr.length);

	    for(int i=0; i<ans.length; i++){
	    
		    System.out.print(ans[i] + " ");
	    }
	    System.out.println();
    }
}
