

// Majority Element II [Leetcode-229]
import java.util.*;
class Solution {
    public List<Integer> majorityElement(int[] nums) {
        List<Integer> lst = new ArrayList<Integer>();
        int n = nums.length;

        if(n==1){
            lst.add(nums[0]);
            return lst;
        }
        if(n==2){
            if(nums[0]==nums[1]){
                lst.add(nums[0]);
            }else{
                lst.add(nums[0]);
                lst.add(nums[1]);
            }
            return lst;
        }
        HashSet<Integer> hs = new HashSet<Integer>();

        for(int i=0; i<n-1; i++){
            int count = 0;
            if(hs.contains(nums[i])){
                continue;
            }else{
                for(int j=i; j<n; j++){
                    if(nums[i]==nums[j]){
                        count++;
                    }
                }
                if(count > n/3){
                    lst.add(nums[i]);
                }
                hs.add(nums[i]);
            }
        }
        return lst;
    }

    public static void main(String[] args){
    
	    int arr[] = new int[]{3,2,3};

	    Solution obj= new Solution();
	    List<Integer> lst = obj.majorityElement(arr);

	    System.out.println(lst);

    }
}
