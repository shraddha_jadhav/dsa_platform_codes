
// Spirally Traversing a Matrix [Leetcode-54, GFG]

import java.util.*;
class Solution{

    static ArrayList<Integer> spirallyTraverse(int matrix[][], int r, int c){
        int row = r;
        int col = c;
        int i=0;
        int j=0;
        ArrayList<Integer> lst = new ArrayList<Integer>();
        if(matrix.length==0){
            return lst;
        }
        if(matrix.length ==1){
            for(j=0; j<matrix[0].length; j++){
                lst.add(matrix[i][j]);
            }
            return lst;
        }
        if(matrix[0].length ==1){
            for(i=0; i<matrix.length; i++){
                lst.add(matrix[i][j]);
            }
            return lst;
        }

        while(row>1 || col >1){
            for(int x=0; x<col-1 && lst.size() < r*c; x++){
                lst.add(matrix[i][j]);
                j++;
            }

            for(int x=0; x<row-1 && lst.size() < r*c; x++){
                lst.add(matrix[i][j]);
                i++;
            }
            for(int x=0; x<col-1 && lst.size() < r*c; x++){
                lst.add(matrix[i][j]);
                j--;
            }

            for(int x=0; x<row-1 && lst.size() < r*c; x++){
                lst.add(matrix[i][j]);
                i--;
            }
            col=col-2;
            row=row-2;
            i++;
            j++;
        }

        if((row==1 || col==1) && lst.size() < r*c){
            lst.add(matrix[i][j]);
        }

         return lst;
    }

    public static void main(String[] args){
    
	int arr[][] = new int[][]{{1,2,3},{4,5,6},{7,8,9}};

	ArrayList<Integer> lst = spirallyTraverse(arr,arr.length,arr[0].length);
	System.out.println(lst);
    }
}

