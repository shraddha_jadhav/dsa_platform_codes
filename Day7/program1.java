
// Check if array is sorted or Rotated

class Solution {
    public boolean check(int[] nums) {
        int count = 0;
        for(int i=0; i<nums.length-1; i++){
            if(nums[i] > nums[i+1]){
                count++;
            }
        }

        if(count > 1){
            return false;
        }

        if(nums[nums.length-1] > nums[0] && count != 0){
            return false;
        }

        return true;
    }

    public static void main(String[] args){
    
	Solution obj = new Solution();

	int nums[] = new int[]{3,4,5,1,2};

	System.out.println(obj.check(nums));
	
    }
}
