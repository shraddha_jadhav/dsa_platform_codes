
// Wave Array

class Solution {

    public static void convertToWave(int n, int[] a) {
     
        for(int i=0; i<n-1; i=i+2){
            if(a[i]<a[i+1]){
                int x = a[i];
                a[i] = a[i+1];
                a[i+1] = x;
            }
        }
    }

    public static void main(String[] args){
    
	    int arr[] = new int[]{1,2,3,4,5};
	    convertToWave(arr.length, arr);

	    for(int i=0; i<arr.length; i++){
	    
		    System.out.print(arr[i] + " ");
	    }

	    System.out.println();
    }
}
