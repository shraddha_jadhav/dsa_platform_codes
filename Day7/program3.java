
// Convert array into Zig-Zag fashion

class Solution {

    public void zigZag(int a[], int n){
        // Code your solution here. 
        for(int i=1; i<n-1; i=i+2){
            if(a[i]<a[i+1]){
                int x = a[i];
                a[i] = a[i+1];
                a[i+1] = x;
            }
            
            if(a[i]<a[i-1]){
                int x = a[i];
                a[i] = a[i-1];
                a[i-1] = x;
            }
            
            if(n%2==0){
                if(a[n-1]<a[n-2]){
                    int x = a[n-2];
                    a[n-1] = a[n-2];
                    a[n-2] = x;
                }
            }
        }
    }
 
    public static void main(String[] args){
    
	    int arr[] = new int[]{4,3,7,8,6,2,1};

	    Solution obj = new Solution();
	    obj.zigZag(arr,arr.length);

	    for(int i=0; i<arr.length; i++){
	    
		    System.out.print(arr[i] + " ");
	    }

	    System.out.println();
    }
}
