
// Intersection of two arrays

import java.util.*;

class Solution {
     
    public int[] intersection(int[] nums1, int[] nums2) {
        
	HashSet<Integer> hs = new HashSet<Integer>();
        for(int i=0; i<nums1.length; i++){
            int flag = 0;
            for(int j=0; j<nums2.length; j++){
                if(nums1[i]==nums2[j]){
                    flag = 1;
                }
            }
            if(flag==1){
                hs.add(nums1[i]);
            }
        }
        int[] ans = new int[hs.size()];
        int i=0;
        for(int val:hs){
            ans[i] = val;
            i++;
        }
        return ans;
    }

    public static void main(String[] args){
    
	    Solution obj = new Solution();

	    int nums1[] = new int[]{1,2,2,1};
	    int nums2[] = new int[]{2,2};

	    int arr[] = obj.intersection(nums1,nums2);

	    for(int i=0; i<arr.length; i++){
	    
		    System.out.print(arr[i] + " ");
	    }
	    System.out.println();
    }
}
