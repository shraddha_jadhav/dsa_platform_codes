
// Length Unsorted Subarray [gfg]

import java.util.*;
class Solve {

    int[] printUnsorted(int[] arr, int n) {
        
        int temp[] = new int[n];
        for(int i=0; i<n; i++){
            temp[i] = arr[i];
        }
        Arrays.sort(temp);

        int ans[] = new int[]{0,0};

        for(int i=0; i<n; i++){
            if(arr[i] != temp[i]){
                ans[0] = i;
                break;
            }
        }
        for(int i=n-1; i>=0; i--){
            if(arr[i] != temp[i]){
                ans[1] = i;
                break;
            }
        }
        return ans;
    }
	
    public static void main(String[] args){
    
	    int arr[] = new int[]{0,1,15,25,6,7,30,40,50};

	    Solve obj = new Solve();
	    int ans[] = obj.printUnsorted(arr,arr.length);

	    for(int i=0; i<ans.length; i++){
	    
		    System.out.print(ans[i] + " ");
	    }
	    System.out.println();
    }
}
