
// Smallest subarray with sum greater than x

class Solution {

    public static int smallestSubWithSum(int a[], int n, int x) {

        int ans = Integer.MAX_VALUE;
        for(int i=0; i<n; i++){
            int sum = 0;
            int count = 0;
            for(int j=i; j<n; j++){
                count++;
                sum= sum +a[j];
                if(sum >x){
                    if(count < ans){
                        ans =  count;
                    }
                    break;
                }
            }
        }

        if(ans != Integer.MAX_VALUE){
            return ans;
        }
        return 0;
    }

    public static void main(String[] args){
    
	    int arr[] = new int[]{1,10,5,2,7};
	    int x = 9;

	    System.out.println(smallestSubWithSum(arr,arr.length,x));
    }
}

