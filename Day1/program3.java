
// Key Pair

import java.util.*;

class Solution {
    boolean hasArrayTwoCandidates(int arr[], int n, int x) {
        // code here
        Arrays.sort(arr);
        int start = 0;
        int end = n-1;

        while(start < end){

            if(arr[start] + arr[end]==x){
                return true;

            }else if(arr[start] + arr[end] < x){
                start++;
            }else{
                end--;
            }
        }
        return false;
    }

    public static void main(String[] args){
    
	    Solution obj = new Solution();

	    int arr[] = new int[]{1,4,45,6,10,8};
	    int x = 16;

	    System.out.println(obj.hasArrayTwoCandidates(arr,arr.length,x));
    }

}
