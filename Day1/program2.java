
// First Repeating Element

import java.util.*;

class Solution {
 
     int firstRepeated(int[] arr, int n) {
 
        int minIdx = -1;   
	HashSet<Integer> hs = new HashSet<Integer>();
        
        for(int i=n-1; i>=0; i--){
            
            if(hs.contains(arr[i])){
                minIdx = i;
            }else{
                hs.add(arr[i]);
            }
        }
        if(minIdx !=-1){
            return minIdx+1;
        }
        return minIdx;
    }

    
    public static void main(String[] args){
        int arr[] = new int[]{1,5,3,4,3,5,6};
        Solution obj = new Solution();
        System.out.println(obj.firstRepeated(arr,arr.length));
    }

}

