
// Single Number
import java.util.*;
class Solution {
    public int singleNumber(int[] nums) {

        HashSet<Integer> hs = new HashSet<Integer>();

        for(int i=0; i<nums.length; i++){
            int count= 0;
            if(hs.contains(nums[i])){
                continue;
            }else{
                for(int j=i; j<nums.length; j++){
                    if(nums[i]==nums[j]){
                        count++;
                    }
                }
                hs.add(nums[i]);
                if(count==1){
                    return nums[i];
                }
            }
        }
        return -1;
    }
    public static void main(String[] args){
        int nums[] = new int[]{2,2,2};
        Solution obj = new Solution();
        System.out.println(obj.singleNumber(nums));
    }
}
