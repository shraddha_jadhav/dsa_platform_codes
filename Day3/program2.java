
// Leaders in an array

import java.util.*;
class Solution{

    static ArrayList<Integer> leaders(int arr[], int n){
    
        ArrayList<Integer> al = new ArrayList<Integer>();
        int max = Integer.MIN_VALUE;
        
        for(int i=n-1; i>=0; i--){
            if(arr[i] >= max){
                al.add(arr[i]);
                max = arr[i];
            }
        }
        Collections.reverse(al);
        return al;
    }

    public static void main(String[] args){
	    int arr[] = new int[]{16,17,4,3,5,2};
    	    System.out.println(leaders(arr,arr.length));
   }
}

