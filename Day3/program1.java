// MAjority Element

import java.util.*;

class Solution
{
    static int majorityElement(int a[], int size) {

        if(size==0){
            return -1;
        }else if(size==1){
            return a[0];
        }else{
            int idx1 = 0;
            int idx2 = 0;
            int count = 0;
            int val = -1;
        
            Arrays.sort(a);
        
            for(int i=0; i<a.length-1; i++){
            
                if(a[i]==a[i+1]){
                    count++;
                }            
                if(count>idx2-idx1){
                    idx2 = i;
                    idx1 = idx2-count+1;
                
                    if(a[i] != a[i+1]){
                        count = 0;
                    }
                    if(count >= size/2){
                        val = a[idx2];
                    }
                }
            }
            return val;
        }
    }	

    public static void main(String[] args){
    
	int a[] = new int[]{6,1,15,19,9,13,12,6,7,2,10,4,1,14,11,14,14,13};

	Arrays.sort(a);

	for(int i=0; i<a.length; i++){
	
		System.out.print(a[i] + " ");
	}
	System.out.println();

	Solution obj = new Solution();
	System.out.println(obj.majorityElement(a,a.length));
    }
}
