
// Two sum
class Solution {
    public int[] twoSum(int[] nums, int target) {
        int arr[] = new int[2];

        for(int i=0; i<nums.length; i++){
            for(int j=i+1; j<nums.length; j++){
                if(nums[i]+nums[j]==target){
                    arr[0]=i;
                    arr[1]=j;
                    return arr;
                }
            }
        }
        return arr;
    }

    public static void main(String[] args){
    
	    int nums[] = new int[]{2,7,3,4};
	    int target = 9;

	    Solution obj = new Solution();
	    int arr[] = obj.twoSum(nums, target);

	    for(int i=0; i<arr.length; i++){
	    
		    System.out.print(arr[i] + " ");
	    }
	    System.out.println();
    }
}
