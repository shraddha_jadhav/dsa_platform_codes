
// find the smallest and second smallest element in the array

import java.util.*;
class Compute
{
    public long[] minAnd2ndMin(long a[], long n)
    {
        long arr[] = new long[2];
        arr[0] = -1;
        arr[1] = -1;
        if(a.length==0 || a.length==1){
            return arr;
        }else{
            Arrays.sort(a);

            for(int i=0; i<a.length-1; i++){

                if(a[i]!=a[i+1]){
                    arr[0] = a[i];
                    arr[1] = a[i+1];
                    break;
                }
            }
        }
        return arr;


    }

    public static void main(String[] args){
    
	Compute obj = new Compute();
	long a[] = new long[]{2,2,3,4,5};
	long arr[] = obj.minAnd2ndMin(a,a.length);

	for(int i=0; i<arr.length; i++){
	
		System.out.print(arr[i] + " ");
	}
	System.out.println();

    }
}

