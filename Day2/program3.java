
// Remove Element

import java.util.*;

class Solution{

	public int removeElement(int nums[], int val){

		int count = 0;

		for(int i=0; i<nums.length; i++){
		
			if(nums[i] != val){
			
				nums[count] = nums[i];
				count++;
			}
		}

		return count;
	}

	public static void main(String[] args){
	
		Solution obj = new Solution();

		int nums[] = new int[]{0,1,2,2,3,0,4,2};
		int val = 2;

		int ret = obj.removeElement(nums,val);

		System.out.println(ret);

		for(int i=0; i<ret; i++){
		
			System.out.print(nums[i] + " ");
		}
		System.out.println();
	}
}
