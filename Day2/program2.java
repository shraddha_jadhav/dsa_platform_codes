
// Remove Element

import java.util.*;

class Solution{

	public int removeElement(int nums[], int val){
	
		int count = 0;
		int count2 = 0;
		for(int i=0; i<nums.length; i++){

			if(nums[i]==0){
			
				count2++;
			}

			if(nums[i]==val){
			
				nums[i] = 0;
				count++;
				continue;
			}

		}

/*		if(nums[nums.length-1]==val){
			nums[nums.length-1] = 0;
			count++;
		}*/

		Arrays.sort(nums);

		for(int i=0; i<nums.length-count-count2; i++){
		
			nums[i+count2] = nums[i+count2+count];
			nums[i+count2+count] = 0;
		}

		return count;
	}

	public static void main(String[] args){
	
		Solution obj = new Solution();

		int nums[] = new int[]{0,1,2,2,3,0,4,2};
		int val = 2;

		int ret = obj.removeElement(nums,val);

		System.out.println(ret);

		for(int i=0; i<nums.length; i++){
		
			System.out.print(nums[i] + " ");
		}
		System.out.println();
	}
}
