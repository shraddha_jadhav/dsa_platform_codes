
// Find Pairs [Leetcode-532]

import java.util.*;
class Solution {
    public int findPairs(int[] nums, int k) {

        int count = 0;
        Arrays.sort(nums);

        int i=0;
        int j=i+1;

        while(j<nums.length && i<nums.length){
            if(i==j){
                j++;
            }else{
                if(nums[j]-nums[i]==k){
                    count++;
                    i++;
                    while(i<nums.length && nums[i]==nums[i-1]){
                        i++;
                    }
                }else if(nums[j]-nums[i]>k){
                    i++;
                }else{
                    j++;
                }
            }
        }
        return count;
    }

    public static void main(String[] args){
    
	    int arr[] = new int[]{1,4,6,2,6,37,78,3,1,4,6};
	    int  k= 2;

	    Solution obj = new Solution();
	    System.out.println(obj.findPairs(arr,k));
    }
}
