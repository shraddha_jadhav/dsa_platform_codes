
// Minimize the height II [gfg]

import java.util.*;
class Solution {
    int getMinDiff(int[] arr, int n, int k) {
        // code here

        Arrays.sort(arr);

        int min = arr[0];
        int max = arr[n-1];
        int ans = max - min;

        for(int i=1; i<n; i++){

            if(arr[i]-k<0){
                continue;
            }

            max = Math.max(arr[n-1]-k,arr[i-1]+k);
            min = Math.min(arr[0]+k,arr[i]-k);

            ans = Math.min(ans,max-min);
        }
        return ans;
    }

    public static void main(String[] args){
    
	    int arr[] = new int[]{2,4,7,2,6,8,2,6,26,8,32};
	    int k = 3;

	    Solution obj = new Solution();
	    System.out.println(obj.getMinDiff(arr,arr.length,k));
    }
}

