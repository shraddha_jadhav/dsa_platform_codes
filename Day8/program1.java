
// merge sorted array

class Solution {
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        if(m==1 && n==1 && nums1[0] == 0){
            nums1[0] = nums2[0];
        }else{

            int arr[] = new int[m];
            for(int i=0; i<m; i++){
                arr[i] = nums1[i];
            }

            int i=0;
            int j =0;
            int k = 0;

            while(i<m && j<n){
                if(arr[i] < nums2[j]){
                    nums1[k] = arr[i];
                    i++;
                }else{
                    nums1[k] = nums2[j];
                    j++;
                }
                k++;
            }

            while(i < m){
                nums1[k] = arr[i];
                i++;
                k++;
            }

            while(j<n){
                nums1[k] = nums2[j];
                j++;
                k++;
            }
        }

	for(int i=0; i<nums1.length; i++){
	
		System.out.print(nums1[i] + " ");
	}

	System.out.println();
    }

    public static void main(String[] args){
    
	    Solution obj = new Solution();
	    int m =6;
	    int n = 3;
	    int nums1[] = new int[]{1,2,3,0,0,0};
	    int nums2[] = new int[]{2,5,6};
	    obj.merge(nums1,m,nums2,n);
    }
}
