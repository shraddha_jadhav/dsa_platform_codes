
// Alternate positive and negative numbers

class Compute {
    public int findElement(int arr[],int n){

        int max = arr[0];
        int idx = arr[1];
        int flag = 0;

        for(int i=1; i<n; i++){
            if(arr[i] >= max){
                max = arr[i];
                if(flag==1 && i!=n-1){
                    idx = arr[i];
                    flag = 0;
                }
            }else{
                flag = 1;
                idx = -1;
            }
        }
        return idx;
    }

    public static void main(String[] args){
    
	    Compute obj = new Compute();
	    int arr[] = new int[]{4,2,5,7};

	    System.out.println(obj.findElement(arr,arr.length));
    }
}
