
import java.util.*;

class Solution {
    void rearrange(int arr[], int n) {
        // code here
        ArrayList<Integer> pos = new ArrayList<Integer>();
        ArrayList<Integer> neg = new ArrayList<Integer>();
        for(int x=0; x<n; x++){
            if(arr[x] >= 0){
                pos.add(arr[x]);
            }else{
                neg.add(arr[x]);
            }
        }
        
        int i=0;
        int j=0;
        int k=0;
        
        while(i<pos.size() && j<neg.size()){
            if(k%2==0){
                arr[k] = pos.get(i);
                i++;
            }else{
                arr[k] = neg.get(j);
                j++;
            }
            
            k++;
        }
        
        while(i<pos.size()){
            arr[k] = pos.get(i);
            i++;
            k++;
        }
        
        while(j<neg.size()){
            arr[k] = neg.get(j);
            j++;
            k++;
        }
    }

    public static void main(String[] args){
    
	    Solution obj = new Solution();

	    int arr[] = new int[]{-15,30,43,-18,-38,38,36,78,-22,-68,16,39,-41,-15,98,69,-72,-32};
	    obj.rearrange(arr,arr.length);

	    for(int i=0; i<arr.length; i++){
	    
		    System.out.print(arr[i] + " ");
	    }
	    System.out.println();
    }
}
