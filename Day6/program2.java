
import java.util.*;

class Solution {
 
    public int maximumProduct(int[] nums) {
        
        if(nums.length < 3){
            return -1;
        }else{
            Arrays.sort(nums);
            int product1 = 1;
            int product2 = 1;
            int N = nums.length;

            product1 = nums[0]*nums[1]*nums[N-1];
            product2 = nums[N-1]*nums[N-2]*nums[N-3];

            if(product1 > product2){
                return product1;
            }else{
                return product2;
            }
     	}
     }	

    public static void main(String[] args){
    
	    int nums[] = new int[]{-1000,-98,-1,2,3,4};

	    Solution obj = new Solution();
	    System.out.println(obj.maximumProduct(nums));
    }
}
