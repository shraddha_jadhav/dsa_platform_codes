
//Remove Duplicates from the sorted array

class Solution {

    public int removeDuplicates(int[] nums) {
        int k = 1;
        for(int i=1; i<nums.length; i++){
            if(nums[i-1] != nums[i]){
                k++;
            }
       }
        int arr[] = new int[k];
        arr[0] = nums[0];
        int j = 1;
        for(int i=1; i<nums.length; i++){
            if(nums[i-1] != nums[i]){
                arr[j] = nums[i];
                j++;
            }
        }

        for(int i=0; i<arr.length; i++){
            nums[i] = arr[i];
        }
        return k;
    }

    public static void main(String[] args){
    
	    int nums[] = new int[]{1,1,2};

	    Solution obj = new Solution();
	    int ret = obj.removeDuplicates(nums);
	    System.out.println(ret);

	    for(int i=0; i<ret; i++){
	    
		    System.out.print(nums[i] + " ");
	    }

	    System.out.println();
    }
}
