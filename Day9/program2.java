
// Apply operations to an array [Leetcode-2460]
import java.util.*;
class Solution {
    public int[] applyOperations(int[] nums) {
        int n = nums.length;
        for(int i=0; i<n-1; i++){
            if(nums[i] == nums[i+1]){
                nums[i] = nums[i]*2;
                nums[i+1] = 0;
            }
        }

        ArrayList<Integer> al = new ArrayList<Integer>();
        for(int i=0; i<n; i++){
            if(nums[i] != 0){
                al.add(nums[i]);
            }
        }

        int j=0;
        for(int i=0; i<n; i++){
            if(j<al.size()){
                nums[i] = al.get(j);
                j++;
            }else{
                nums[i] = 0;
            }
        }
        return nums;
    }

    public static void main(String[] args){
    
	    Solution obj = new Solution();

	    int arr[] = new int[]{1,2,2,1,1,0};
	    obj.applyOperations(arr);

	    for(int i=0; i<arr.length; i++){
	    
		    System.out.print(arr[i] + " ");
	    }

	    System.out.println();
    }
}
