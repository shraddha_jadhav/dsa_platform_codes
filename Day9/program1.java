
// Missing number [Leetcode-268]

class Solution {
    public int missingNumber(int[] nums) {

        int n = nums.length;

        int sum = n*(n+1)/2;

        for(int i=0;i<n; i++){

            sum = sum-nums[i];
        }

        return sum;
    }

    public static void main(String[] args){
    
	    Solution obj = new Solution();

	    int nums[] = new int[]{0,1,3};

	    System.out.println(obj.missingNumber(nums));
    }
}
