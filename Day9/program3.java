
// Buildings receiving sunlight [gfg]

class Solution {

    public static int longest(int arr[],int n) {
        int count = 1;
        int max = arr[0];

        for(int i=1; i<n; i++){

            if(max<=arr[i]){
                count++;
                max = arr[i];
            }
        }
        return count;
    }

    public static void main(String[] args){
    
	    int arr[] = new int[]{6,2,8,4,11,13};

	    System.out.println(longest(arr,arr.length));
    }
}

