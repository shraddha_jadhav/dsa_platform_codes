class Solution {
    static void reArrange(int[] arr, int N) {
        // code here
        int even = 0;
        int odd = 1;
        
        while(even<N && odd < N){
            if(arr[even]%2==0){
                even = even + 2;
            }else if(arr[odd]%2==1){
                odd = odd + 2;
            }else{
                int temp = arr[even];
                arr[even] = arr[odd];
                arr[odd] = temp;
                
                even = even+2;
                odd = odd + 2;
            }
        }
    }

    public static void main(String[] args){
    
	    int arr[] = new int[]{1,2,5,3,6,8};

	    reArrange(arr,arr.length);

	    for(int i=0; i<arr.length; i++){
	    
		    System.out.print(arr[i] + " ");
	    }
	    System.out.println();
    }
}
