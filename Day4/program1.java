
// Max consecutive once

class Solution {

    public int findMaxConsecutiveOnes(int[] nums) {
        int count1 = 0;
        int count2 = 0;
        for(int i=0;i<nums.length;i++){

            if(nums[i]==1 ){
                count1++;
            }
            if(count1>count2){
                count2 = count1;
            }
            if(nums[i]==0){
                count1 = 0;
            }
        }
        return count2;
    }

    public static void main(String[] args){
    
	Solution obj = new Solution();
	int nums[] = new int[]{1,1,0,1,1,1};

	System.out.println(obj.findMaxConsecutiveOnes(nums));
    }
}
