
// Next Greater Element I

class Solution {
    public int[] nextGreaterElement(int[] nums1, int[] nums2) {
        int arr[] = new int[nums1.length];

        for(int i=0; i<nums1.length; i++){
            int flag = 0;
            for(int j=0; j<nums2.length; j++){
                if(nums1[i]==nums2[j]){
                    flag = 1;
                }

                if(flag==1 && nums1[i] < nums2[j]){
                    arr[i] = nums2[j];
                    break;
                }
            }
        }
        for(int i=0; i<arr.length; i++){
            if(arr[i]==0){
                arr[i] = -1;
            }
        }

        return arr;
    }

    public static void main(String[] args){
    
	    Solution obj = new Solution();

	    int num1[] = new int[]{4,1,2};
	    int num2[] = new int[]{1,3,4,2};
	
	    int arr[] = obj.nextGreaterElement(num1,num2);

	    for(int i=0; i<arr.length; i++){
	    
		    System.out.print(arr[i] + " ");
	    }
	    System.out.println();
    }
}
