
// Find the Duplicats Number

class Solution {
    public int findDuplicate(int[] nums) {
	int max = 0;
	for(int i=0; i<nums.length; i++){
	
		if(max < nums[i]){
		
			max = nums[i];
		}
	}
        int ans[] = new int[max+1];
        for(int i=0; i<nums.length; i++){
            ans[nums[i]]++;
        }
        int a = 0;
        for(int i=0; i<ans.length; i++){
            if(ans[i]>1){
                a=i;
            }
        }
        return a;
    }

    public static void main(String[] args){
    
	    int arr[] = new int[]{1,2,4,2};

	    Solution obj = new Solution();
	    System.out.println(obj.findDuplicate(arr));
    }
}
