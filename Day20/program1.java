
// Find all Duplicates in Array
import java.util.*;
class Solution {
    public List<Integer> findDuplicates(int[] nums) {
        List<Integer> lst = new ArrayList<Integer>();
        Arrays.sort(nums);
        for(int i=0; i<nums.length-1; i++){
            if(nums[i]==nums[i+1]){
                if(!lst.contains(nums[i])){
                    lst.add(nums[i]);
                }
            }
        }
        return lst;
    }

    public static void main(String[] args){
    
	    int nums[] = new int[]{1,2,5,2,3,2};

	    Solution obj = new Solution();
	    List<Integer> lst = obj.findDuplicates(nums);

	    System.out.println(lst);
    }
}
