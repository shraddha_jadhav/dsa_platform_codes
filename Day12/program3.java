
// left most and right most index (gfg)

class pair  {
    long first, second;
    public pair(long first, long second)
    {
        this.first = first;
        this.second = second;

	System.out.println(first + "," + second);
    }

    public static void main(String[] args){
    
    }
}

class Solution {

    public pair indexes(long v[], long x) {
        // Your code goes here

        int first = -1;
        int last = -1;
        int flag = 0;
        for(int i=0; i<v.length; i++){

            if(v[i]==x && flag==0){
                first = i;
                last = i;
                flag = 1;
            }

            if(v[i]==x){
                last = i;
            }
        }
        return new pair(first,last);
    }

    public static void main(String[] args){
    
	    Solution obj = new Solution();

	    long arr[] = new long[]{1,2,3,4,5,5,5,6,7,8,9,10};
	    long x = 5;

	    obj.indexes(arr,x);
    }
}
