
// Moves all zeros to the end of the array [Leetcode,gfg]

class Solution {
    public void moveZeroes(int[] nums) {

        int idx = 0;
        for(int i=0; i<nums.length; i++){
            if(nums[i] != 0){
                nums[idx] = nums[i];
                idx++;
            }
        }

        while(idx<nums.length){
            nums[idx] = 0;
            idx++;
        }
    }

    public static void main(String[] args){
    
	    int nums[] = new int[]{1,4,0,3,0,2,0,0};

	    Solution obj = new Solution();
	    obj.moveZeroes(nums);

	    for(int i=0; i<nums.length; i++){
	    
		    System.out.print(nums[i] + " ");
	    }

	    System.out.println();
    }
}
