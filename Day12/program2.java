
// Product of maximum in first array and minimum in second. (gfg)

class Solution{

    public static long find_multiplication (int arr[], int brr[], int n, int m) {
  
        int max = Integer.MIN_VALUE;

        for(int i=0; i<n; i++){
            if(arr[i] > max){
                max = arr[i];
            }
        }

        int min = Integer.MAX_VALUE;
        for(int i=0; i<m; i++){
            if(brr[i]<min){
                min = brr[i];
            }
        }

        return (long)(max*min);
    }

    public static void main(String[] args){
    
	    int arr[] = new int[]{1,2,4,6,7};
	    int brr[] = new int[]{4,5,1,6,8,2,};

	    System.out.println(find_multiplication(arr,brr,arr.length,brr.length));
    }
}

