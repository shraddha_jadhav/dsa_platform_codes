
// Binary Search [gfg]

class Solution {
    int binarysearch(int arr[], int n, int k) {
 
        int start = 0;
        int end = arr.length-1;

        while(start <= end){
            int mid = start + (end-start)/2;

            if(arr[mid]==k){
                return mid;
            }else if(arr[mid] > k){
                end = mid-1;
            }else{
                start = mid+1;
            }
        }
        return -1;
    }

    public static void main(String[] args){
    
	    Solution obj = new Solution();

	    int arr[] = new int[]{1,2,3,4,6,7,8,56};
	    int k = 8;

	    System.out.println(obj.binarysearch(arr,arr.length,k));
    }
}
