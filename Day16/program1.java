
// Best time to buy and sell stock [Leetcode-121]

class Solution {
    public int maxProfit(int[] prices) {
        int max = 0;
        int min = prices[0];
        for(int i=1; i<prices.length; i++){
            if(max < (prices[i]-min)){
                max = prices[i]-min;
            }
            if(prices[i] < min){
                min = prices[i];
            }
        }
        return max;
    }

    public static void main(String[] args){
    
	    Solution obj = new Solution();

	    int arr[] = new int[]{7,1,5,6,3,4};

	    System.out.println(obj.maxProfit(arr));
    }
}
