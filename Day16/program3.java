
// Bitonic Point [gfg]

class Solution {
    int findMaximum(int[] arr, int n) {

        for(int i=0; i<n-1; i++){
            if(arr[i] > arr[i+1]){
                return arr[i];
            }
        }

        return arr[n-1];
    }

    public static void main(String[] args){
    
	    int arr[] = new int[]{1,15,25,45,42,21,17,12,11};

	    Solution obj = new Solution();
	    System.out.println(obj.findMaximum(arr,arr.length));
    }
}
