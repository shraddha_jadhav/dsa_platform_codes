
// Row with max 1's

class Solution {
    int rowWithMax1s(int arr[][], int n, int m) {
 
        int max = 0;
        int j = m-1;

        for(int i=0; i<n; i++){

            while(j >=0 && arr[i][j]==1){
                j= j-1;
                max = i;
            }
        }

        if(max==0 && arr[0][m-1]==0){
            return -1;
        }
        return max;
    }

    public static void main(String[] args){
    
	  int arr[][] = new int[][]{{0,0,1,1},{1,1,1,1},{0,1,1,1},{1,1,1,1}};

	  Solution obj = new Solution();

	  System.out.println(obj.rowWithMax1s(arr,arr.length,arr[0].length));
    }
}
