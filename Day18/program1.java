
// Sort Colors [Leetcode-75]

class Solution {
    public void sortColors(int[] nums) {

        for(int i=0; i<nums.length-1; i++){
            for(int j=i+1; j<nums.length; j++){
                if(nums[j] < nums[i]){
                    int temp = nums[j];
                    nums[j] = nums[i];
                    nums[i] = temp;
                }
            }
        }
    }

    public static void main(String[] args){
    
	    int nums[] = new int[]{2,1,0,1,0,2,0,1};

	    Solution obj = new Solution();
	    obj.sortColors(nums);

	    for(int i=0; i<nums.length; i++){
	    
		    System.out.print(nums[i] + " ");
	    }

	    System.out.println();
    }
}
