/*
 *Q2: Find maximum sum path involving elements of given arrays.
 Given two sorted arrays of integers, find a maximum sum path 
 involving elements of both arrays whose sum is maximun. 
 We can start from either array, but we can switch between arrays 
 only through its common elements.
 * */

import java.io.*;
public class MaxSumPath {

    public static int findMaxSum(int[] X, int[] Y) {
        int m = X.length;
        int n = Y.length;
        int i = 0, j = 0;
        int sumOfX = 0, sumOfY = 0;
        int result = 0;

 
        while (i < m && j < n) {
   	     
            if (X[i] == Y[j]) {
		    result += Math.max(sumOfX, sumOfY) + X[i];

                    sumOfX = 0;
                    sumOfY = 0;
		    i++;
                    j++;
            } else if (X[i] < Y[j]) {
		    sumOfX += X[i];
		    i++;
            } else {
		    sumOfY += Y[j];
		    j++;
            }
        }


        while (i < m) {
		sumOfX += X[i];     // Add remaining elements of X
           	i++;
	}

        while (j < n) {
            	sumOfY += Y[j];     //Add remaining elements of Y
	    	j++;              
        }

        result += Math.max(sumOfX, sumOfY);

        return result;
    }

    public static void main(String[] args)throws IOException {
	    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	    System.out.println("Enter size of the X and Y arrays");
	    int n = Integer.parseInt(br.readLine());
	    int m = Integer.parseInt(br.readLine());
       	    int X[] = new int[n];                        // {3, 6, 7, 8, 10, 12, 15, 18, 100};
            int Y[] = new int[m];                        //{1, 2, 3, 5, 7, 9, 10, 11, 15, 16, 18, 25, 50};

	    System.out.println("Enter 'X' array elements");
	    for(int i=0; i<n; i++){
	    
		    X[i] = Integer.parseInt(br.readLine());
	    }

	    System.out.println("Enter 'Y' array elements");
            for(int i=0; i<m; i++){

                    Y[i] = Integer.parseInt(br.readLine());
            }

            int maxSum = findMaxSum(X, Y);
            System.out.println("Maximum sum path: " + maxSum);
    }
}

