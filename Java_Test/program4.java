
// Write a java program to find the duplicate characters in a string.

import java.util.*;
import java.io.*;
class DuplicateCharacter{

	void duplicateChar(String str){
	
		str = str.replaceAll("\\s", "");

                HashSet<Character> hs=new HashSet<Character>();

                for (int i = 0; i < str.length(); i++) {
                        for (int j = i + 1; j < str.length(); j++) {
                                if (str.charAt(i) == str.charAt(j)) {
                                        hs.add(str.charAt(i));
                                        break;
                                }
                        }
                }

                Iterator<Character> it = hs.iterator();

                System.out.println("Duplicate Characters are:");

                while (it.hasNext()) {

                        System.out.println(it.next()+" ");

                }

	}

	public static void main(String[] args)throws IOException{
	
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        	System.out.println("Enter the given string");
        	String str=br.readLine();

		DuplicateCharacter obj = new DuplicateCharacter();
		obj.duplicateChar(str);
	}
}
