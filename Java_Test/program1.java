
/*
 * Q1: Merge two arrays by satisfying given constraints.
 * Given two sorted arrays X[] and Y[] of size and n each
 *  where > n and X[] has exactly n vacant cells, merge elements 
 *  of Y[] in their correct position in array X[], i.e., merge (X, Y) 
 *  by keeping the sorted order.
 *
*/

import java.util.*;

class MergeArrays {
   
    static void mergeArrays(int[] X, int[] Y) {
        int m = X.length;
        int n = Y.length;
        int index = m - 1;

        for (int i = m - 1; i >= 0; i--) {
            if (X[i] != 0) {
                X[index] = X[i];
                index--;
            }
        }

        int i = index + 1;
        int j = 0;
        int k = 0;
        while (i < m && j < n) {
            if (X[i] < Y[j]) {
                X[k++] = X[i++];
            } else {
                X[k++] = Y[j++];
            }
        }

        while (j < n) {
            X[k++] = Y[j++];
        }
    }

    public static void main(String[] args) {
        
	Scanner scanner = new Scanner(System.in);
        
	System.out.println("Enter the size of array X:");
        int m = scanner.nextInt();
        int[] X = new int[m];
        
	System.out.println("Enter the elements of array X:");
        for (int i = 0; i < m; i++) {
            X[i] = scanner.nextInt();
        }
        
	System.out.println("Enter the size of array Y:");
        int n = scanner.nextInt();
        int[] Y = new int[n];
        
	System.out.println("Enter the elements of array Y:");
        for (int i = 0; i < n; i++) {
            Y[i] = scanner.nextInt();
        }
        
	mergeArrays(X, Y);
        System.out.println("Merged Array: " + Arrays.toString(X));

        scanner.close();
    }

}
