//  Write a java program to count the number of words in a string using HashMap.

import java.util.*;

public class NoOfWords {

    public static Map<String, Integer> countOfWords(String str) {
        Map<String, Integer> wordsCount = new HashMap<>();

        if (str == null || str.isEmpty()) {
            return wordsCount;
        }

        String[] words = str.trim().split("\\s+");

        for (String word : words) {
            wordsCount.put(word, wordsCount.getOrDefault(word, 0) + 1);
        }

        return wordsCount;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter  string:");
        String str = scanner.nextLine();

        Map<String, Integer> wordsCount = countOfWords(str);
        int totalCount = 0;
        for (int count : wordsCount.values()) {
            totalCount += count;
        }

        System.out.println("Total number of words: " + totalCount);
        scanner.close();
    }

}


