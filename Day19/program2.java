
// Number of Zero-Filled Subarrays [Leetcode,gfg]

class Solution{
	long no_of_subarrays(int N, int [] arr) {
		//Write your code here
		int len = 0;
		int sum = 0;
		long count = 0;

		for(int i=0; i<N; i++){
		    sum = sum + arr[i];
		    if(sum != 0){
		        sum = 0;
		        len = 0;
		        continue;
		    }
		    len++;
		    count = count + len;
		}
		return count;
	}

	public static void main(String[] args){
	
		int arr[] = new int[]{1,0,0,4,5,0,0,2};

		Solution obj = new Solution();

		System.out.println(obj.no_of_subarrays(arr.length,arr));
	}
}
