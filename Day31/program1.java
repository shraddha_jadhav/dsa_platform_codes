
// Triplet Sum in Array [gfg]
import java.util.*;
class Solution{
       	public static boolean find3Numbers(int A[], int n, int X) {
	       	if (n < 3)
     		 return false;

 		Arrays.sort(A);

    		for (int i = 0; i < n-2; i++) {
      			if (i > 0 && A[i] == A[i - 1])
        			continue;

      			int l = i + 1;
      			int r = n - 1;
      			while (l < r) {
        			int sum = A[i] + A[l] + A[r];
        			if (sum == X) {
            				return true;
        			} else if (sum < X) {
          				++l;
       				} else {
          				--r;
       				}
      			}
    		}

    		return false;
  	}

	public static void main(String[] args){
	
		int arr[] = new int[]{1,2,3,4,5,67,8};
		int X = 10;
	
		System.out.println(find3Numbers(arr,arr.length,X));
	}
}


