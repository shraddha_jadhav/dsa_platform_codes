
// 3Sum [Leetcode-15]
import java.util.*;
class Solution {
    public List<List<Integer>> threeSum(int[] nums) {

    if (nums.length < 3)
      return new ArrayList<>();

    List<List<Integer>> ans = new ArrayList<>();

    Arrays.sort(nums);

    for (int i = 0; i + 2 < nums.length; ++i) {
      if (i > 0 && nums[i] == nums[i - 1])
        continue;

      int l = i + 1;
      int r = nums.length - 1;
      while (l < r) {
        int sum = nums[i] + nums[l] + nums[r];
        if (sum == 0) {
          ans.add(Arrays.asList(nums[i], nums[l++], nums[r--]));
          while (l < r && nums[l] == nums[l - 1])
            ++l;
          while (l < r && nums[r] == nums[r + 1])
            --r;
        } else if (sum < 0) {
          ++l;
        } else {
          --r;
        }
      }
    }

    return ans;
  }

  public static void main(String[] args){
  
	  int nums[] = new int[]{1,2,3,5,6,8,45,3,4,87};

	  Solution obj = new Solution();
	  List<List<Integer>> ans = obj.threeSum(nums);

	  System.out.println(ans);
  }
}

