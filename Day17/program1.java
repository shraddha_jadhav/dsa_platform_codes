
// Kth largest element [Leetcode-215]
import java.util.*;
class Solution {
	
	public int findKthLargest(int[] nums, int k) {
	        Arrays.sort(nums);

        	if(nums.length < k){
            		return -1;
        	}
        	return nums[nums.length-k];
    	}

	public static void main(String[] args){
	
		Solution obj = new Solution();

		int nums[] = new int[]{3,5,2,7,1,88,4,21,55,32,56};

		System.out.println(obj.findKthLargest(nums,3));
	}
}
