
// Kth smallest element [gfg]

import java.util.*;
class Solution{
    public static int kthSmallest(int[] arr, int l, int r, int k){
   
        Arrays.sort(arr);

        return arr[l+k-1];
    }

    public static void main(String[] args){
    
	    int arr[] = new int[]{3,5,27,1,8,7,4,2};
	    int k = 5;

	    System.out.println(kthSmallest(arr,0,arr.length-1,k));
    }
}

