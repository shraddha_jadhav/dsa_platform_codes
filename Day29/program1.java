
// Rotate Image [Leetcode-48]

class Solution {
    public void rotate(int[][] matrix) {
        int n = matrix.length;

        for(int i=0; i<n; i++){
            for(int j=i+1; j<n; j++){
                int temp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = temp;
            }
        }

        for(int i=0; i<n; i++){
            int start =0;
            int end = n-1;

            while(start<end){
                int temp = matrix[i][start];
                matrix[i][start] = matrix[i][end];
                matrix[i][end] = temp;
                start++;
                end--;
            }
        }
    }

    public static void main(String[] args){
    
	    int arr[][] = new int[][]{{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20},{21,22,23,24,25}};

	    Solution obj = new Solution();
	    obj.rotate(arr);

	    for(int i=0; i<arr.length; i++){
	    
		    for(int j=0; j<arr[0].length; j++){
		    
			    System.out.print(arr[i][j]+ " ");
		    }
		    System.out.println();
	    }
    }
}
