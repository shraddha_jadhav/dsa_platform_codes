
// K-th element of two Arrays [gfg]

class Solution {
    public long kthElement( int arr1[], int arr2[], int n, int m, int k) {
        int arr3[] = new int[n+m];
        int itr1 = 0;
        int itr2 = 0;
        int itr3 = 0;
        while(itr1<n && itr2<m){

            if(arr1[itr1] < arr2[itr2]){
                arr3[itr3] = arr1[itr1];
                itr1++;
            }else{
                arr3[itr3] = arr2[itr2];
                itr2++;
            }

            itr3++;
        }

        while(itr1 <n){
            arr3[itr3] = arr1[itr1];
            itr1++;
            itr3++;
        }

        while(itr2<m){
            arr3[itr3] = arr2[itr2];
            itr2++;
            itr3++;
        }
        return (long)arr3[k-1];
    }

    public static void main(String[] args){
    
	    int arr1[] = new int[]{3,6,7,8,9,14,22};
	    int arr2[] = new int[]{1,2,5,11,12,16,20};
	    int k = 8;

	    Solution obj = new Solution();
	    System.out.println(obj.kthElement(arr1,arr2,arr1.length,arr2.length,k));
    }
}
