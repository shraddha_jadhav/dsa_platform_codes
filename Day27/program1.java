
// Maximum Product Subarray [Leetcode-152]

class Solution {
    public int maxProduct(int[] nums) {

        if(nums.length==1){
            return nums[0];
        }
        int product = 1;
        int maxProd = 0;
        for(int i=0; i<nums.length; i++){
            product *= nums[i];

            if(maxProd < product){
                maxProd = product;
            }

            if(maxProd < nums[i]){
                maxProd = nums[i];
            }

            if(product == 0){
                product = 1;
            }
        }

        product = 1;
        for(int i=nums.length-1; i>=0; i--){
            product *= nums[i];

            if(maxProd < product){
                maxProd = product;
            }

            if(maxProd < nums[i]){
                maxProd = nums[i];
            }

            if(product==0){
                product = 1;
            }
        }
        return maxProd;
    }

    public static void main(String[] args){
    
	    int arr[] = new int[]{2,4,-5,-1,2,-3,1,8,};

	    Solution obj = new Solution();
	    System.out.println(obj.maxProduct(arr));

    }
}
